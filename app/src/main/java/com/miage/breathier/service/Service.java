package com.miage.breathier.service;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

//Classe abstraite pour avoir les SharedPreferences dans le service
public abstract class Service {

    protected SharedPreferences sharedPreferences;
    private Context parentContext;

    public Service(Context parentContext){
        this.parentContext = parentContext;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(parentContext);
    }
}
