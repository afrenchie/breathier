package com.miage.breathier.service;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miage.breathier.event.EventBusManager;
import com.miage.breathier.event.MeasurementSearchResultEvent;
import com.miage.breathier.model.Measurement;
import com.miage.breathier.model.MeasurementSearchResult;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

//Service pour la recherche de mesures
public class MeasurementSearchService {

    private static final long REFRESH_DELAY = 650;
    public static MeasurementSearchService INSTANCE = new MeasurementSearchService();
    private final MeasurementSearchService.MeasurementSearchRESTService mMeasurementSearchRESTService; //
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;
    private final String order_by  = "date";
    private final String sort  = "desc";

    private MeasurementSearchService() {
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();
        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl("https://api.openaq.org/v1/")
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();
        mMeasurementSearchRESTService = retrofit.create(MeasurementSearchService.MeasurementSearchRESTService.class);
    }

    //Fonction de recherche dans l'API des locations par location
    public void searchMeasurementFromLocation(final String search) {
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }
        mLastScheduleTask = mScheduler.schedule(new Runnable() {
            public void run() {
                mMeasurementSearchRESTService.searchForMeasurementByLocation(search, order_by, sort).enqueue(new Callback<MeasurementSearchResult>() {
                    @Override
                    public void onResponse(Call<MeasurementSearchResult> call, Response<MeasurementSearchResult> response) {
                        if (response.body() != null && response.body().results != null) {
                            try {
                                ActiveAndroid.beginTransaction();
                                for (Measurement measurement : response.body().results) {
                                    measurement.city = measurement.city.replace("\'", " ");
                                    measurement.location = measurement.location.replace("\'", " ");
                                    if(measurement.date != null && measurement.date.utc != null) {
                                        measurement.dateUtc = measurement.date.utc;
                                    }
                                    else
                                        measurement.dateUtc = "";
                                    measurement.save();
                                }
                                ActiveAndroid.setTransactionSuccessful();
                                ActiveAndroid.endTransaction();

                            } catch (Exception e){}

                            searchMeasurementsFromDBByLocation(search);
                        } else {
                            searchMeasurementsFromDBByLocation(search);
                        }

                    }

                    @Override
                    public void onFailure(Call<MeasurementSearchResult> call, Throwable t) {
                        searchMeasurementsFromDBByLocation(search);
                    }
                });
            }
        }, REFRESH_DELAY, TimeUnit.MILLISECONDS);
    }


    //Fonction de recherche dans l'API des locations par city
    public void searchMeasurementFromCity(final String search) {
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }

        mLastScheduleTask = mScheduler.schedule(new Runnable() {
            public void run() {
                mMeasurementSearchRESTService.searchForMeasurementByCity(search, order_by, sort).enqueue(new Callback<MeasurementSearchResult>() {
                    @Override
                    public void onResponse(Call<MeasurementSearchResult> call, Response<MeasurementSearchResult> response) {
                        if (response.body() != null && response.body().results != null) {
                            try {
                                ActiveAndroid.beginTransaction();
                                for (Measurement measurement : response.body().results) {
                                    measurement.city = measurement.city.replace("\'", " ");
                                    measurement.location = measurement.location.replace("\'", " ");

                                    if(measurement.date != null && measurement.date.utc != null) {
                                        measurement.dateUtc = measurement.date.utc;
                                    }
                                    else{
                                        measurement.dateUtc = "";
                                    }
                                    measurement.save();
                                }
                                ActiveAndroid.setTransactionSuccessful();
                                ActiveAndroid.endTransaction();

                            } catch (Exception e){}
                        }
                        searchMeasurementsFromDBByCity(search);
                    }

                    @Override
                    public void onFailure(Call<MeasurementSearchResult> call, Throwable t) {
                        searchMeasurementsFromDBByCity(search);
                    }
                });
            }
        }, REFRESH_DELAY, TimeUnit.MILLISECONDS);
    }

    /*
    order_by suivi de groupby ne fonctionne pas dans sqlite
    La requete voulue :
    SELECT *
        FROM (
            SELECT * FROM Measurement
            WHERE location = 'FR23001'
            ORDER BY dateUtc DESC
        ) AS sub
        GROUP BY parameter;
    Le problème c'est l'ORM ne permet pas (encore) de faire de telles requêtes
    Du coup extrait directement les paramètres
    */
    //Fonction de recherche en local des mesures par location
    private void searchMeasurementsFromDBByLocation(String location) {
        String searchLocation = location.replace("\'", " ");
        List<Measurement> matchingMeasurementsFromDB = new Select().
                from(Measurement.class)
                .where("location = '" + searchLocation + "'")
                .orderBy("dateUtc DESC")
                //.groupBy("parameter") marche po
                .execute();
        EventBusManager.BUS.post(new MeasurementSearchResultEvent(getFreshest(matchingMeasurementsFromDB)));
    }

    //Fonction de recherche en local des mesures par city
    private void searchMeasurementsFromDBByCity(String city) {
        String searchCity = city.replace("\'", " ");
        List<Measurement> matchingMeasurementsFromDB = new Select().
                from(Measurement.class)
                .where("city = '" + searchCity + "'")
                .orderBy("dateUtc DESC")
                .execute();
        EventBusManager.BUS.post(new MeasurementSearchResultEvent(getFreshest(matchingMeasurementsFromDB)));
    }

    //Du coup on choppe les mesures les plus "récentes"
    private List<Measurement> getFreshest(List<Measurement> matchingMeasurementsFromDB){
        List<Measurement> res = new ArrayList<>();
        List<String> params = new ArrayList<>();
        for(Measurement m : matchingMeasurementsFromDB){
            if(!params.contains(m.parameter)) {
                params.add(m.parameter);
                res.add(m);
            }
        }
        return res;
    }

    public interface MeasurementSearchRESTService {
        @GET("measurements/")
        Call<MeasurementSearchResult> searchForMeasurementByCity(@Query("city") String city, @Query("order_by") String order_by, @Query("sort") String sort);

        @GET("measurements/")
        Call<MeasurementSearchResult> searchForMeasurementByLocation(@Query("location") String location, @Query("order_by") String order_by, @Query("sort") String sort);
    }
}
