package com.miage.breathier.service;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miage.breathier.event.CountrySearchResultEvent;
import com.miage.breathier.event.EventBusManager;
import com.miage.breathier.model.Area;
import com.miage.breathier.model.Country;
import com.miage.breathier.model.CountrySearchResult;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

//Service pour la recherche de pays
public class CountrySearchService {
    private static final long REFRESH_DELAY = 650;
    public static CountrySearchService INSTANCE = new CountrySearchService();
    private final CountrySearchRESTService mCountrySearchRESTService;
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;

    private CountrySearchService() {
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();
        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl("https://api.openaq.org/v1/")
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();
        mCountrySearchRESTService = retrofit.create(CountrySearchRESTService.class);
    }

    //Permet de chercher des pays dans l'API et de les enregistrer/rechercher dans la base en local
    public void searchCountriesFromCountry() {
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }

        mLastScheduleTask = mScheduler.schedule(new Runnable() {
            public void run() {
                mCountrySearchRESTService.searchForCountries("110").enqueue(new Callback<CountrySearchResult>() {
                    @Override
                    public void onResponse(Call<CountrySearchResult> call, Response<CountrySearchResult> response) {
                        if (response.body() != null && response.body().results != null) {
                            try {
                                ActiveAndroid.beginTransaction();
                                for (Country country : response.body().results) {
                                    country.save();
                                }
                                ActiveAndroid.setTransactionSuccessful();
                                ActiveAndroid.endTransaction();
                            } catch (Exception e){}
                            getCountriesFromDB();
                        } else {}

                    }
                    @Override
                    public void onFailure(Call<CountrySearchResult> call, Throwable t) {
                        getCountriesFromDB();
                    }
                });
            }
        }, REFRESH_DELAY, TimeUnit.MILLISECONDS);
    }
    //Fonction de recherche en local des pays (country)
    private void searchCountryFromDBByName(String name) {
        List<Country> matchingPlacesFromDB = new Select().
                from(Area.class)
                .where("name LIKE '%" + name + "%'")
                .orderBy("name")
                .execute();
        EventBusManager.BUS.post(new CountrySearchResultEvent(matchingPlacesFromDB));
    }

    //Fonction de récupération en local de tous les pays
    private void getCountriesFromDB() {
        List<Country> matchingPlacesFromDB = new Select().
                from(Area.class)
                .orderBy("name")
                .execute();
        EventBusManager.BUS.post(new CountrySearchResultEvent(matchingPlacesFromDB));
    }

    public interface CountrySearchRESTService {
        @GET("countries/")
        Call<CountrySearchResult> searchForCountries(@Query("limit") String limit);
    }

}
