package com.miage.breathier.service;

import android.content.Context;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miage.breathier.event.AreaSearchResultEvent;
import com.miage.breathier.event.EventBusManager;
import com.miage.breathier.model.Area;
import com.miage.breathier.model.AreaSearchResult;

import java.lang.reflect.Modifier;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

//Service pour la recherche de zones
public class AreaSearchService extends Service{

    public static AreaSearchService INSTANCE;
    private final AreaSearchRESTService mAreaSearchRESTService;
    private static final long REFRESH_DELAY = 650;
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;

    //Obligé d'utiliser le pattern singleton si on veut utiliser les sharedPreferences dans l'instance de service
    public static AreaSearchService getInstance(Context parentContext){
        if(INSTANCE != null)
            return INSTANCE;
        else {
            INSTANCE = new AreaSearchService(parentContext);
            return INSTANCE;
        }
    }

    private AreaSearchService(Context parentContext) {
        super(parentContext);
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();
        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl("https://api.openaq.org/v1/")
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();
        mAreaSearchRESTService = retrofit.create(AreaSearchRESTService.class);
    }

    //Permet de chercher des zones dans l'API et de les enregistrer/rechercher dans la base en local
    public void searchAreasFromCountry(final String search) {
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }

        //On regarde quand a été maj la liste des zones
        long currentDateTime = Calendar.getInstance().getTime().getTime()/1000;
        long lastLocationUpdate = sharedPreferences.getLong("last_location_update", 0);

        if(lastLocationUpdate == 0 || lastLocationUpdate - currentDateTime > 900){
            mLastScheduleTask = mScheduler.schedule(new Runnable() {
                public void run() {
                    // Call to the REST service
                    mAreaSearchRESTService.searchForAreas("FR", "150").enqueue(new Callback<AreaSearchResult>() {
                        @Override
                        public void onResponse(Call<AreaSearchResult> call, Response<AreaSearchResult> response) {
                            if (response.body() != null && response.body().results != null) {
                                try {
                                    ActiveAndroid.beginTransaction();
                                    for (Area area : response.body().results) {
                                        area.save();
                                    }
                                    ActiveAndroid.setTransactionSuccessful();
                                    ActiveAndroid.endTransaction();
                                    sharedPreferences.edit().putLong("last_location_update", Calendar.getInstance().getTime().getTime()/1000).apply();
                                } catch (Exception e){}
                            }
                            getAllAreasFromDB();
                        }
                        @Override
                        public void onFailure(Call<AreaSearchResult> call, Throwable t) {
                            getAllAreasFromDB();
                        }
                    });
                }
            }, REFRESH_DELAY, TimeUnit.MILLISECONDS);
        } else {
            //Quand il y a qqchose à chercher, on cherche
            if(search != null && !search.isEmpty()){
                searchAreasFromDBByName(search);
            }
            //sinon on affiche tout
            else
                getAllAreasFromDB();
        }

    }

    //Fonction de recherche en local des zones par city (name)
    private void searchAreasFromDBByName(String name) {
        List<Area> matchingAreasFromDB = new Select().
                from(Area.class)
                .where("name LIKE '%" + name + "%'")
                .orderBy("name ASC")
                .execute();
        EventBusManager.BUS.post(new AreaSearchResultEvent(matchingAreasFromDB));
    }

    //Fonction pour avoir toutes les zones enregistrées en local
    private void getAllAreasFromDB() {
        List<Area> matchingAreasFromDB = new Select().
                from(Area.class)
                .orderBy("name ASC")
                .execute();
        EventBusManager.BUS.post(new AreaSearchResultEvent(matchingAreasFromDB));
    }

    public interface AreaSearchRESTService {
        @GET("cities/")
        Call<AreaSearchResult> searchForAreas(@Query("country") String search, @Query("limit") String limit);
    }
}
