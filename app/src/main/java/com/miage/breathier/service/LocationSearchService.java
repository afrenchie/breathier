package com.miage.breathier.service;

import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miage.breathier.event.EventBusManager;
import com.miage.breathier.event.LocationSearchResultEvent;
import com.miage.breathier.model.Location;
import com.miage.breathier.model.LocationSearchResult;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

//Service pour la recherche de localisations
public class LocationSearchService {
    private static final long REFRESH_DELAY = 650;
    public static LocationSearchService INSTANCE = new LocationSearchService();
    private final LocationSearchRESTService mLocationSearchRESTService;
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;

    private LocationSearchService() {
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();
        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl("https://api.openaq.org/v1/")
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();
        mLocationSearchRESTService = retrofit.create(LocationSearchRESTService.class);
    }


    public void searchFavoriteLocations() {
        searchFavoriteLocationsFromDB();
    }

    //Pour la recherche de location en local
    public void searchLocations(final String search) {
        if(search != null && !search.isEmpty()) {
            searchLocationsFromDB(search);
        } else {
            //On ne va pas lancer une recherche pour ne rien trouver
            EventBusManager.BUS.post(new LocationSearchResultEvent(new ArrayList<>()));
        }
    }

    //Permet de chercher des localisations dans l'API et de les enregistrer/rechercher dans la base en local
    public void searchLocationFromCity(final String search) {
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }

        mLastScheduleTask = mScheduler.schedule(new Runnable() {
            public void run() {
                mLocationSearchRESTService.searchForLocationByCity(search).enqueue(new Callback<LocationSearchResult>() {
                    @Override
                    public void onResponse(Call<LocationSearchResult> call, Response<LocationSearchResult> response) {
                        if (response.body() != null && response.body().results != null) {
                            try {
                                ActiveAndroid.beginTransaction();
                                for (Location location : response.body().results) {
                                    if(location.coordinates != null){
                                        location.longitude = location.coordinates.longitude;
                                        location.latitude = location.coordinates.latitude;
                                    }
                                    if(location.parameters != null && location.parameters.size() > 0){
                                        location.parametersConcat = location.parameters.toString();
                                    }
                                    String lc = "";
                                    if(location.cities != null && location.cities.size() > 0){
                                        for(String c : location.cities){
                                            lc = lc.concat(";").concat(c.replace('\'',' '));
                                        }
                                        lc = lc.concat(";");
                                        location.citiesConcat = lc;
                                    } else {
                                        //Certaines dénominations ont un ' ce qui fait crasher l'ORM
                                        lc = location.city.replace('\'',' ');
                                        location.citiesConcat = ";".concat(lc).concat(";");
                                    }
                                    location.cityOrigin = search.replace('\'',' ');
                                    location.save();
                                }
                                ActiveAndroid.setTransactionSuccessful();
                                ActiveAndroid.endTransaction();
                            } catch (Exception e){
                                Log.e("DEBUG", "FOIRAGE Enregistrement en db : " + e.getMessage());
                            }
                            searchLocationsFromDBByCity(search);
                        } else {}
                    }

                    @Override
                    public void onFailure(Call<LocationSearchResult> call, Throwable t) {
                        searchLocationsFromDBByCity(search);
                    }
                });
            }
        }, REFRESH_DELAY, TimeUnit.MILLISECONDS);
    }

    //Fonction de recherche en local des locations favorites
    private void searchFavoriteLocationsFromDB() {
        List<Location> matchingLocationsFromDB = new Select().
                from(Location.class)
                .where("location IN (SELECT location FROM Favorite)")
                .execute();
        EventBusManager.BUS.post(new LocationSearchResultEvent(matchingLocationsFromDB));
    }

    //Fonction de recherche en local des locations par city/location
    private void searchLocationsFromDB(String search) {
        String searchStuff = search.replace("\'", " ");
        //Gros trick global dans le where
        List<Location> matchingLocationsFromDB = new Select().
                from(Location.class)
                .where("cityOrigin LIKE '%" + searchStuff + "%' OR location LIKE '%" + searchStuff + "%'")
                .execute();
        EventBusManager.BUS.post(new LocationSearchResultEvent(matchingLocationsFromDB));
    }

    //Fonction de recherche en local des locations par city
    private void searchLocationsFromDBByCity(String city) {
        String searchCity = city.replace("\'", " ");
        List<Location> matchingLocationsFromDB = new Select().
                from(Location.class)
                .where("cityOrigin == '" + searchCity + "'")
                .where("country == 'FR'")
                .orderBy("city")
                .execute();
        EventBusManager.BUS.post(new LocationSearchResultEvent(matchingLocationsFromDB));
    }

    public interface LocationSearchRESTService {
        @GET("locations/")
        Call<LocationSearchResult> searchForLocationByCity(@Query("city") String search);
    }
}



