package com.miage.breathier;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

/*
* Affiche diverses infos sur la qualité de l'air
* */
public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
