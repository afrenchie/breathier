package com.miage.breathier;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    /*Composants pour le magnifique fond qui bouge*/
    @BindView(R.id.background_one)
    ImageView backgroundOne;

    @BindView(R.id.background_two)
    ImageView backgroundTwo;
    ValueAnimator animator;

    private SharedPreferences sharedPreferences;

    /*Fonction pour initier le formidable fond qui bouge*/
    public void startBG(){
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(100000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float progress = (float) animation.getAnimatedValue();
                final float width = backgroundOne.getWidth();
                final float translationX = width * progress;
                backgroundOne.setTranslationX(translationX);
                backgroundTwo.setTranslationX(translationX - width);
            }
        });
        animator.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        super.onCreate(savedInstanceState);

        String loc_country = sharedPreferences.getString("loc_country", null);
        //On veut vraiment que ce soit la France
        if(loc_country == null){
            loc_country = "FR";
            sharedPreferences.edit().putString("loc_country", loc_country).apply();
        }
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        /*Initialisation du composant animator du sublime fond qui bouge*/
        animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        startBG();
    }
    //Avec plusieurs onclick on arrive à faire ouvrir plusieurs activités à la fois, c'est marrant
    //@OnClick({R.id.main_areas_list_layout, R.id.main_areas_list_image, R.id.main_areas_list_button})
    @OnClick(R.id.main_areas_list_button)
    public void clickedOnMainSearchArea() {
        Intent intent = new Intent(this, AreaListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.main_about_button)
    public void clickedOnMainAbout() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.main_settings_button)
    public void clickedOnMainSettings() {
        Intent intent = new Intent(this, ParametersActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.main_search_location_button)
    public void clickedOnMainSearchLocation() {
        Intent intent = new Intent(this, LocationSearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.main_favorites_location_button)
    public void clickedOnMainFavoriteLocation() {
        Intent intent = new Intent(this, FavoriteLocationActivity.class);
        startActivity(intent);
    }
}
