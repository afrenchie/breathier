package com.miage.breathier;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.miage.breathier.event.AreaSearchResultEvent;
import com.miage.breathier.event.EventBusManager;
import com.miage.breathier.event.LocationSearchResultEvent;
import com.miage.breathier.service.AreaSearchService;
import com.miage.breathier.service.LocationSearchService;
import com.miage.breathier.ui.AreaAdapter;
import com.miage.breathier.ui.LocationAdapter;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/*Vue pour la recherche des localisation enregistrées en local
* */
public class LocationSearchActivity extends AppCompatActivity {
    private LocationAdapter mLocationAdapter;

    @BindView(R.id.location_search_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.location_search_search_bar)
    EditText mSearchEditText;

    @BindView(R.id.location_search_progress_bar)
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_search);
        ButterKnife.bind(this);

        mLocationAdapter = new LocationAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mLocationAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                mProgressBar.setVisibility(View.VISIBLE);
                LocationSearchService.INSTANCE.searchLocations(editable.toString());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        LocationSearchService.INSTANCE.searchLocations(mSearchEditText.getText().toString());
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void searchResult(final LocationSearchResultEvent event) {
        runOnUiThread (() -> {
            mLocationAdapter.setLocations(event.getLocations());
            mLocationAdapter.notifyDataSetChanged();
            mProgressBar.setVisibility(View.GONE);
        });
    }
}
