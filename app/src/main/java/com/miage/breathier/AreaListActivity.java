package com.miage.breathier;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.miage.breathier.event.AreaSearchResultEvent;
import com.miage.breathier.event.EventBusManager;
import com.miage.breathier.service.AreaSearchService;
import com.miage.breathier.ui.AreaAdapter;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Affiche la liste des 135 zones
 * */
public class AreaListActivity extends AppCompatActivity {

    private AreaAdapter mAreaAdapter;

    @BindView(R.id.area_list_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.area_list_search_bar)
    EditText mSearchEditText;

    @BindView(R.id.area_list_progress_bar)
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_list);
        ButterKnife.bind(this);

        mAreaAdapter = new AreaAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mAreaAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mProgressBar.setVisibility(View.VISIBLE);
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                mProgressBar.setVisibility(View.VISIBLE);
                AreaSearchService.getInstance(getApplicationContext()).searchAreasFromCountry(editable.toString());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        AreaSearchService.getInstance(getApplicationContext()).searchAreasFromCountry(mSearchEditText.getText().toString());
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void searchResult(final AreaSearchResultEvent event) {
        runOnUiThread (() -> {
            mAreaAdapter.setAreas(event.getAreas());
            mAreaAdapter.notifyDataSetChanged();
            mProgressBar.setVisibility(View.GONE);
        });

    }
}
