package com.miage.breathier;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.miage.breathier.event.EventBusManager;
import com.miage.breathier.event.LocationSearchResultEvent;
import com.miage.breathier.service.LocationSearchService;
import com.miage.breathier.ui.LocationAdapter;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/*Affiche les localisations favorites
* */
public class FavoriteLocationActivity extends AppCompatActivity {

    private LocationAdapter mLocationAdapter;

    @BindView(R.id.favorite_location_recycler_view)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_location);

        ButterKnife.bind(this);
        mLocationAdapter = new LocationAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mLocationAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        LocationSearchService.INSTANCE.searchFavoriteLocations();
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void searchResult(final LocationSearchResultEvent event) {
        runOnUiThread (() -> {
            mLocationAdapter.setLocations(event.getLocations());
            mLocationAdapter.notifyDataSetChanged();
        });
    }
}
