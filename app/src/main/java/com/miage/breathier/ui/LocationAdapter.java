package com.miage.breathier.ui;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.miage.breathier.LocationDetailsActivity;
import com.miage.breathier.R;
import com.miage.breathier.model.Location;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//Adapter pour les localisations
public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder> {
    private LayoutInflater inflater;
    private Activity context;
    private List<Location> mLocations;

    public LocationAdapter(Activity context, List<Location> locations) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.mLocations = locations;
    }

    @Override
    public LocationAdapter.LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.location_item, parent, false);
        LocationAdapter.LocationViewHolder holder = new LocationAdapter.LocationViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(LocationAdapter.LocationViewHolder holder, int position) {
        final Location location = mLocations.get(position);
        holder.mLocationItemLocationTextView.setText(String.valueOf(location.location));
        holder.mLocationItemSourceNameTextView.setText(String.valueOf(location.sourceName));

        holder.mLocationItemLocationTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent seeLocationDetailsIntent = new Intent(context, LocationDetailsActivity.class);
                seeLocationDetailsIntent.putExtra("location", location.location);
                seeLocationDetailsIntent.putExtra("longitude", location.longitude);
                seeLocationDetailsIntent.putExtra("latitude", location.latitude);
                context.startActivity(seeLocationDetailsIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLocations.size();
    }

    public void setLocations(List<Location> locations) {
        this.mLocations = locations;
    }

    class LocationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.location_item_location)
        TextView mLocationItemLocationTextView;

        @BindView(R.id.location_item_source_name)
        TextView mLocationItemSourceNameTextView;

        public LocationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}



