package com.miage.breathier.ui;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.miage.breathier.R;
import com.miage.breathier.model.Measurement;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//Adapter pour les mesures
public class MeasurementAdapter extends RecyclerView.Adapter<MeasurementAdapter.MeasurementViewHolder> {

    private final double pm25_limit = 25;
    private final double pm10_limit = 40;
    private final double so2_limit = 125;
    private final double no2_limit = 40;
    private final double o3_limit = 120;
    private final double co_limit = 10000;
    private final double bc_limit = 25;

    private LayoutInflater inflater;

    private Activity context;

    private List<Measurement> mMeasurements;

    public MeasurementAdapter(Activity context, List<Measurement> measurements) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.mMeasurements = measurements;
    }

    @Override
    public MeasurementAdapter.MeasurementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.measurement_item, parent, false);
        MeasurementAdapter.MeasurementViewHolder holder = new MeasurementAdapter.MeasurementViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MeasurementAdapter.MeasurementViewHolder holder, int position) {
        final Measurement measurement = mMeasurements.get(position);
        //On affiche l'icone danger si la limite moyenne a été atteinte !
        Boolean limit_reached = false;
        switch (measurement.parameter) {
            case "pm25":
                limit_reached = measurement.value > pm25_limit;
                break;
            case "pm10":
                limit_reached = measurement.value > pm10_limit;
                break;
            case "so2":
                limit_reached = measurement.value > so2_limit;
                break;
            case "no2":
                limit_reached = measurement.value > no2_limit;
                break;
            case "o3":
                limit_reached = measurement.value > o3_limit;
                break;
            case "co":
                limit_reached = measurement.value > co_limit;
                break;
            case "bc":
                limit_reached = measurement.value > bc_limit;
                break;
            default:
                break;
        }
        if(limit_reached)
            holder.mMeasurementItemNameImageView.setVisibility(View.VISIBLE);
        holder.mMeasurementItemNameTextView.setText(measurement.parameter);
        holder.mMeasurementItemValueTextView.setText(measurement.getValuePerUnit());
    }

    public void setMeasurements(List<Measurement> measurements) {
        this.mMeasurements = measurements;
    }

    @Override
    public int getItemCount() {
        return mMeasurements.size();
    }

    class MeasurementViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.limit_pic)
        ImageView mMeasurementItemNameImageView;

        @BindView(R.id.measurement_item_name)
        TextView mMeasurementItemNameTextView;

        @BindView(R.id.measurement_item_value)
        TextView mMeasurementItemValueTextView;

        public MeasurementViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
