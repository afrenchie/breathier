package com.miage.breathier.ui;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.miage.breathier.AreaDetailActivity;
import com.miage.breathier.R;
import com.miage.breathier.model.Area;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//Adapter pour les zones
public class AreaAdapter extends RecyclerView.Adapter<AreaAdapter.AreaViewHolder> {
    private LayoutInflater inflater;
    private Activity context;
    private List<Area> mAreas;

    public AreaAdapter(Activity context, List<Area> areas) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.mAreas = areas;
    }

    @Override
    public AreaAdapter.AreaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.area_item, parent, false);
        AreaAdapter.AreaViewHolder holder = new AreaAdapter.AreaViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(AreaAdapter.AreaViewHolder holder, int position) {
        final Area area = mAreas.get(position);
        holder.mAreaItemNameTextView.setText(String.valueOf(area.name));
        holder.mAreaItemLocationTextView.setText(String.valueOf(area.locations));

        holder.mAreaItemNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent seeAreaDetailIntent = new Intent(context, AreaDetailActivity.class);
                seeAreaDetailIntent.putExtra("city", area.name);
                context.startActivity(seeAreaDetailIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mAreas.size();
    }

    public void setAreas(List<Area> areas) {
        this.mAreas = areas;
    }

    class AreaViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.area_item_name)
        TextView mAreaItemNameTextView;

        @BindView(R.id.area_item_locations_number)
        TextView mAreaItemLocationTextView;

        public AreaViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}