package com.miage.breathier;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.miage.breathier.event.EventBusManager;
import com.miage.breathier.event.LocationSearchResultEvent;
import com.miage.breathier.event.MeasurementSearchResultEvent;
import com.miage.breathier.service.LocationSearchService;
import com.miage.breathier.service.MeasurementSearchService;
import com.miage.breathier.ui.LocationAdapter;
import com.miage.breathier.ui.MeasurementAdapter;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Affiche les détails d'une zone
 * */
public class AreaDetailActivity extends AppCompatActivity {

    @BindView(R.id.area_detail_city)
    TextView mAreaDetailCity;

    @BindView(R.id.area_detail_measurements_recycler_view)
    RecyclerView mMeasurementsRecyclerView;

    @BindView(R.id.area_detail_locations_recycler_view)
    RecyclerView mLocationsRecyclerView;

    @BindView(R.id.area_detail_progress_bar)
    ProgressBar mProgressBar;

    private LocationAdapter mLocationAdapter;
    private MeasurementAdapter mMeasurementAdapter;

    private String mCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_detail);

        ButterKnife.bind(this);

        mCity = getIntent().getStringExtra("city");
        mAreaDetailCity.setText(mCity);

        mLocationAdapter = new LocationAdapter(this, new ArrayList<>());
        mLocationsRecyclerView.setAdapter(mLocationAdapter);
        mLocationsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mMeasurementAdapter = new MeasurementAdapter(this, new ArrayList<>());
        mMeasurementsRecyclerView.setAdapter(mMeasurementAdapter);
        mMeasurementsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);

        MeasurementSearchService.INSTANCE.searchMeasurementFromCity(mCity);
        LocationSearchService.INSTANCE.searchLocationFromCity(mCity);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void searchLocationResult(final LocationSearchResultEvent event) {
        runOnUiThread (() -> {
            mLocationAdapter.setLocations(event.getLocations());
            mLocationAdapter.notifyDataSetChanged();
            mProgressBar.setVisibility(View.GONE);
        });
    }

    @Subscribe
    public void searchMeasurementResult(final MeasurementSearchResultEvent event) {
        runOnUiThread (() -> {
            mMeasurementAdapter.setMeasurements(event.getMeasurements());
            mMeasurementAdapter.notifyDataSetChanged();
            mProgressBar.setVisibility(View.GONE);
        });
    }
}
