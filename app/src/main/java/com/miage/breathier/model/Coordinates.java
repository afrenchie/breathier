package com.miage.breathier.model;

import com.google.gson.annotations.Expose;

public class Coordinates {
    public double latitude = 0;
    public double longitude = 0;
}
