package com.miage.breathier.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;

import java.util.List;

public class AreaSearchResult {
    @Expose
    public List<Area> results;

    @NonNull
    @Override
    public String toString() {
        return results.toString();
    }
}
