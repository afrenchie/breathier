package com.miage.breathier.model;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.util.Date;
import java.util.List;

/*
 * Modèle représentant les locations
 * Ces dernières sont stockées en local dans la table portant le même nom
 */
@Table(name = "Location")
public class Location extends Model {

    //Location identifier
    @Expose
    @Column(name = "location", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String location;

    //Country containing location in 2 letter ISO code
    @Expose
    @Column(name = "country", index = true)
    public String country;

    //City containing location
    @Expose
    @Column(name = "city", index = true)
    public String city;

    @Expose
    public List<String> cities;

    @Expose
    @Column(name = "citiesConcat")
    public String citiesConcat = "";

    @Expose
    @Column(name = "cityOrigin")
    public String cityOrigin = "";

    //Number of measurements, cumulative by specificity level
    @Expose
    @Column(name = "count")
    public int count;

    //Can follow this to determine active adapter used for this location
    @Expose
    @Column(name = "sourceName")
    public String sourceName;

    //When was data first grabbed for this location (in UTC)?
    @Expose
    @Column(name = "firstUpdated")
    public Date firstUpdated;

    //When was data last grabbed for this location (in UTC)?
    @Expose
    @Column(name = "lastUpdated")
    public Date lastUpdated;

    //List of parameters present for this location
    @Expose
    public List<String> parameters;

    @Expose
    @Column(name = "parametersConcat")
    public String parametersConcat = "";

    //Coordinate of location
    public Coordinates coordinates = null;

    //Coordinate latitude of location
    @Expose
    @Column(name = "latitude")
    public double latitude;

    //Coordinate longitude of location
    @Expose
    @Column(name = "longitude")
    public double longitude;

    @Override
    public String toString(){
        return location.concat(" ")
                .concat(sourceName)
                .concat(" ")
                .concat(city)
                .concat(" ")
                .concat(country)
                .concat(" ")
                .concat(citiesConcat)
                .concat(" ")
                .concat(cityOrigin);
    }

}

