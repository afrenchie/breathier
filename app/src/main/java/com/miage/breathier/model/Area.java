package com.miage.breathier.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.sql.Date;

/*
* Modèle représentant les 'zones' ou 'cities'.
* Ces dernières sont stockées en local dans la table portant le même nom
*/
@Table(name = "Area")
public class Area extends Model {

    //Country containing city, in 2 letter ISO code
    @Expose
    @Column(name = "country")
    public String country;

    //Name of the city
    @Expose
    @Column(name = "name", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String name;

    //Number of measurements for this city
    @Expose
    @Column(name = "count")
    public int count;

    //Number of locations in this city
    @Expose
    @Column(name = "locations")
    public int locations;

    @Expose
    @Column(name = "latitude")
    public int latitude = 0;

    @Expose
    @Column(name = "longitude")
    public int longitude = 0;

    @Expose
    @Column(name = "locationsLastUpdate")
    public Date locationsLastUpdate;

    public String toString(){
        return this.country
                .concat(" ")
                .concat(this.country)
                .concat(" ")
                .concat(this.name)
                .concat(" ")
                .concat(String.valueOf(this.count));
    }
}
