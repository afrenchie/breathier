package com.miage.breathier.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

@Table(name = "Country")
public class Country extends Model {

    @Expose
    @Column(name = "code", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    String code;

    @Expose
    @Column(name = "count")
    int count;

    @Expose
    @Column(name = "locations")
    int locations;

    @Expose
    @Column(name = "cities")
    int cities;

    @Expose
    @Column(name = "name")
    String name;
}
