package com.miage.breathier.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

@Table(name = "Measurement")
public class Measurement extends Model {
    @Expose
    @Column(name = "country")
    public String country;

    @Expose
    @Column(name = "location")
    public String location;

    @Expose
    @Column(name = "city")
    public String city;

    @Expose
    @Column(name = "parameter", uniqueGroups = {"group1"}, onUniqueConflicts = Column.ConflictAction.REPLACE)
    public String parameter;

    @Expose
    @Column(name = "unit")
    public String unit;

    @Expose
    @Column(name = "value")
    public double value;

    @Expose
    public ApiDate date;

    @Expose
    @Column(name = "dateUtc", index = true, uniqueGroups = {"group1"}, onUniqueConflicts = Column.ConflictAction.REPLACE)
    public String dateUtc;

    public String getValuePerUnit(){
        return String.valueOf(value).concat(unit);
    }

    @Override
    public String toString(){
        String res = country
                .concat(" ")
                .concat(city)
                .concat(" ")
                .concat(location)
                .concat(" ")
                .concat(parameter)
                .concat(" ")
                .concat(String.valueOf(value))
                .concat(unit)
                .concat(" ")
                .concat(dateUtc==null?"NULL":dateUtc);
        return res;
    }
}
