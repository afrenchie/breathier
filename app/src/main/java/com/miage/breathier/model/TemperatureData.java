package com.miage.breathier.model;

import com.activeandroid.annotation.Column;
import com.google.gson.annotations.Expose;

public class TemperatureData {

    @Expose
    @Column(name = "location")
    public String location;

    @Expose
    @Column(name = "requestDateTime")
    public Long requestDateTime;

    @Expose
    @Column(name = "dateTime")
    public Long dateTime;

    @Expose
    @Column(name = "date")
    public String date;

    @Expose
    @Column(name = "temperature")
    public double temperature;

    @Expose
    @Column(name = "latitude")
    public double latitude = 0;

    @Expose
    @Column(name = "longitude")
    public double longitude = 0;
}
