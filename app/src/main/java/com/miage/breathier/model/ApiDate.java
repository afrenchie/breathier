package com.miage.breathier.model;

import com.google.gson.annotations.Expose;

import java.util.Date;

public class ApiDate {
    @Expose
    public String utc;

    @Expose
    public String local;

    public String toString(){
        return "ApiDateutc : ".concat(utc);
    }
}
