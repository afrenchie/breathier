package com.miage.breathier.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class MeasurementSearchResult {
    @Expose
    public List<Measurement> results;
}
