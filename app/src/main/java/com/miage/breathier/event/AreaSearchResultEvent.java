package com.miage.breathier.event;

import com.miage.breathier.model.Area;

import java.util.List;

public class AreaSearchResultEvent {
    private List<Area> areas;

    public AreaSearchResultEvent(List<Area> areas) {
        this.areas = areas;
    }

    public List<Area> getAreas() {
        return areas;
    }
}
