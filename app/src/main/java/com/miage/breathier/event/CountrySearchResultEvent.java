package com.miage.breathier.event;

import com.miage.breathier.model.Country;

import java.util.List;

public class CountrySearchResultEvent {

    private List<Country> countries;

    public CountrySearchResultEvent(List<Country> countries) {
        this.countries = countries;
    }

    public List<Country> getAreas() {
        return countries;
    }
}
