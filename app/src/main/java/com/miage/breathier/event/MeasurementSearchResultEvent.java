package com.miage.breathier.event;

import com.miage.breathier.model.Measurement;

import java.util.List;

public class MeasurementSearchResultEvent {
    private List<Measurement> measurements;

    public MeasurementSearchResultEvent(List<Measurement> measurements) {
        this.measurements = measurements;
    }

    public List<Measurement> getMeasurements() {
            return measurements;
        }
}
