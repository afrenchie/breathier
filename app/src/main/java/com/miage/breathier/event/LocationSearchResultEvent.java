package com.miage.breathier.event;

import com.miage.breathier.model.Location;

import java.util.List;

public class LocationSearchResultEvent {
    private List<Location> locations;

    public LocationSearchResultEvent(List<Location> locations) {
        this.locations = locations;
    }

    public List<Location> getLocations() {
        return locations;
    }
}
