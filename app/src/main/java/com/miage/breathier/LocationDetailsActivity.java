package com.miage.breathier;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.activeandroid.ActiveAndroid;
import com.miage.breathier.event.EventBusManager;
import com.miage.breathier.event.MeasurementSearchResultEvent;
import com.miage.breathier.model.Favorite;
import com.miage.breathier.service.MeasurementSearchService;
import com.miage.breathier.ui.MeasurementAdapter;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/*Affiche le détail d'une localisation
* */
public class LocationDetailsActivity extends AppCompatActivity {

    @BindView(R.id.location_detail_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.location_detail_name)
    TextView mLocationDetailName;

    private String mLocation;

    private MeasurementAdapter mMeasurementAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_details);

        ButterKnife.bind(this);
        mLocation = getIntent().getStringExtra("location");
        mLocationDetailName.setText(mLocation);

        mMeasurementAdapter = new MeasurementAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mMeasurementAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @OnClick(R.id.location_detail_favorite)
    public void clickedOnFavorite() {
        Favorite favorite = new Favorite();
        favorite.location = this.mLocation;
        try{
            ActiveAndroid.beginTransaction();
            favorite.save();
            ActiveAndroid.setTransactionSuccessful();
            ActiveAndroid.endTransaction();
        } catch (Exception e){}
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        MeasurementSearchService.INSTANCE.searchMeasurementFromLocation(mLocation);
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void searchResult(final MeasurementSearchResultEvent event) {
        runOnUiThread (() -> {
            mMeasurementAdapter.setMeasurements(event.getMeasurements());
            mMeasurementAdapter.notifyDataSetChanged();
        });
    }
}
